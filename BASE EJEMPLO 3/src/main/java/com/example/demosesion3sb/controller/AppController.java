package com.example.demosesion3sb.controller;

import com.example.demosesion3sb.modelo.Persona;
import com.example.demosesion3sb.repo.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("")
    ModelAndView index() {
        List<Persona> personas = personaRepository.findAll();

        return new ModelAndView("index")
                .addObject("personas", personas);
    }

    @GetMapping("/nuevo")
    ModelAndView nuevo() {
        return new ModelAndView("nuevo")
                .addObject("persona", new Persona());
    }

    @PostMapping("/nuevo")
    ModelAndView crear(@Validated Persona persona, BindingResult bindingResult, RedirectAttributes ra) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("nuevo")
                    .addObject("persona", persona);
        }
        personaRepository.save(persona);
        ra.addFlashAttribute("success", "La persona ha sido creada correctamente");
        return new ModelAndView("redirect:/");
    }

}
