package com.example.demosesion3sb.modelo;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona")
    private Integer id;

    @NotBlank
    private String nombre;

    @NotNull
    private Integer edad;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Sexo sexo;

    private boolean casado;


    public enum Sexo {
        M,
        F
    }
}
