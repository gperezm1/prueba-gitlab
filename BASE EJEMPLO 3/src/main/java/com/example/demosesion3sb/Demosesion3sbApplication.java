package com.example.demosesion3sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demosesion3sbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demosesion3sbApplication.class, args);
	}

}
